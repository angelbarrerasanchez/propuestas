# Social Justice Code Repositories

Un pequeño análisis de las teorías del software libre y cómo se incardinan en las teorías de la justicia respecto a la producción cultural y de conocimiento.

## Formato de la propuesta

Indicar uno de estos:

* [x] Charla (25 minutos)
* [ ] Charla relámpago (10 minutos)

## Descripción

¿Garantiza el software libre que el usuario sea libre? ¿Son necesarios y suficientes los principios del FSM para garantizar la justicia? ¿Cuál es el rol de los repositorios de código en la promoción de la justicia en el software? Estas son algunas de las preguntas que intentaremos desgranar en la charla con el propósito principal de analizar bajo qué condiciones el Free Software Movement funciona para alcanzar la justicia en la producción, uso y consumo de software. Así, la base de la charla será retar el funcionamiento de las tesis de Richard Stallman como teoría, poniendo en valor el rol de los repositorios de código abierto y buscando los puntos débiles que deben ser mejorados.

Si bien la charla presenta un punto de vista teórico, pretende ponerle sentido al FSM a través de diferentes teorías de la justicia preguntandose cómo el FSM puede ser clave en la libertad del usuario sin quedarse en papel mojado.

## Público objetivo

La charla está dirigida a todo aquél interesado en acercarse a las virtudes del software libre y esté dispuesto a cuestionarse los postulados clásicos de la misma.

## Ponente(s)

Consultora de seguridad y cumplimiento en ElevenPaths. Jurista y politóloga por la Universidad Carlos III de Madrid. Master en Gobierno de las Telecomunicaciones por la London School of Economics. Especialista en Derecho Tecnológico e Informática Forense. Editora del medio de análisis político y social Polikracia. Ha centrado su carrera profesional en la seguridad en organizaciones, aunque está especializada en derecho tecnológico, protección de datos, seguridad informática y su relación con las ciencias sociales y el derecho. En este sentido, investiga la interrelación entre política y tecnología desde distintos planos como los derechos fundamentales, la privacidad o las relaciones internacionales. Algunos de sus proyectos versan sobre infraestructuras críticas y seguridad internacional; seguridad y privacidad en sistemas de voto electrónico o la concepción de la privacidad en el plano social.

### Contacto(s)

* Nombre: Lorena Sánchez Chamorro
* Contacto: lor.sanchez.chamorro@gmail.com

## Comentarios

## Condiciones

* [x] Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
* [x] Al menos una persona entre los que la proponen estará presente el día programado para la charla.
